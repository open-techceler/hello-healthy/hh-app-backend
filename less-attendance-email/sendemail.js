var mysql = require('mysql');
var config = require('./config.json')
var aws = require('aws-sdk');

aws.config.loadFromPath('./config.json');


var ses = new aws.SES({
   region: 'us-east-1'
});

var pool = mysql.createPool({
    host : config.host,
    port : config.port,
    user : config.user,
    password : config.password,
    database : config.database
});

var eParams = {
    Destination: {
        ToAddresses: ["team@hellohealthy.com"]
    },
    Message: {
        Body: {
            Html: {
                Data: "Hey! What is up?"
            }
        },
        Subject: {
            Data: "Email Subject!!!"
        }
    },
    Source: "team@hellohealthy.com"
};

function go() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
    
    pool.getConnection(function(error,connection) {
        
        console.log('===Before querying===');

        pool.query('select EVENT_NAME,EVENT_ID,NUMBER_OF_ATTENDEES,ANSWER_ADDITIONAL_FEEDBACK,CLUB,YEAR,MONTH,DAY from EVENT_INSTRUCTOR_FEEDBACK where number_of_attendees < 5 and DATE_CREATED >= DATE_SUB(NOW(),INTERVAL 30 MINUTE)',function (err,result){
            console.log('===after querying===');
            connection.release();   
            console.log('===after release===');
         for (var i=0; i < result.length; i++) { 

            if (result[i].MONTH=='01'){
                result[i].MONTH = "January";
            }
            if (result[i].MONTH=='02'){
                result[i].MONTH = "February";
            }
            if (result[i].MONTH=='03'){
                result[i].MONTH = "March";
            }
            if (result[i].MONTH=='04'){
                result[i].MONTH = "April";
            }
            if (result[i].MONTH=='05'){
                result[i].MONTH = "May";
            }
            if (result[i].MONTH=='06'){
                result[i].MONTH = "June";
            }
            
            if (result[i].MONTH=='07'){
                result[i].MONTH = "July";
            }
            
            if (result[i].MONTH=='08'){
                result[i].MONTH = "August";
            }
            
            if (result[i].MONTH=='09'){
                result[i].MONTH = "September";
            }
            
            if (result[i].MONTH=='10'){
                result[i].MONTH = "October";
            }
            
            if (result[i].MONTH=='11'){
                result[i].MONTH = "November";
            }
            
            if (result[i].MONTH=='12'){
                result[i].MONTH = "December";
            }
            eParams.Message.Subject.Data = "Low Attendance at "+ result[i].CLUB +" for " + result[i].EVENT_NAME+ " held on "+ result[i].MONTH + ", " + result[i].DAY +" " + result[i].YEAR;   
            eParams.Message.Body.Html.Data = result[i].NUMBER_OF_ATTENDEES+" people attended " + result[i].EVENT_NAME + " at "+ result[i].CLUB +" held on " + result[i].MONTH + ", "+ result[i].DAY +" " + result[i].YEAR + 
            "<p>Please review provider feedback, resident feedback, and current average attendance to ensure this doesn't continue.</p>"
            + "<p>"+"<b>Feedback from the Instructor:</b><br>"+result[i].ANSWER_ADDITIONAL_FEEDBACK+"<p>";

            console.log(result);
            var email = ses.sendEmail(eParams, function(err, data){
                if(err) console.log(err);
                else {
                    console.log("===EMAIL SENT===");
                    console.log(data);
            
                    console.log("EMAIL CODE END");
                    console.log('EMAIL: ', email);
                    //callback(null, email);
                }
            });
        }    
        if (error) console.log(error);
        else console.log(result);

        resolve(result);
    });
});

},2500)
})

};


let index = function index(event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    var promise = go(); 
    promise.then(function(value) {
        console.log(value);
        callback(null, value);

      });
}
    exports.handler = index;
