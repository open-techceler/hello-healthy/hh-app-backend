var mysql = require('mysql');
var config = require('./config.json')

var pool = mysql.createPool({
    host : config.host,
    port : config.port,
    user : config.user,
    password : config.password,
    database : config.database
});

exports.handler = (event,context,callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    pool.getConnection(function(err,connection) {

        var year = event.params.querystring.year;
        var club = event.params.querystring.club;
        club = club.replace(/%20/g, " ");

        var eventType = event.params.querystring.eventType;
        if (eventType == "All"){
            
            var sql = "select count(*) as classes, sum(number_of_attendees) as attendance, sum(number_of_attendees)/count(*) as ave, month from HelloHealthyDatabase.EVENT_INSTRUCTOR_FEEDBACK "+ 
                "where year = ? "+
                "and club = ? "+
                "group by month";

            connection.query(sql,[year,club],function (err,result){
            connection.release();   

            if (err) callback(err);
            else callback(null,result);
            });
                
        }
        else {
        var sql = "select count(*) as classes, sum(number_of_attendees) as attendance, sum(number_of_attendees)/count(*) as ave, month from HelloHealthyDatabase.EVENT_INSTRUCTOR_FEEDBACK "+ 
                "where year = ? "+
                "and club = ? "+
                "and event_type = ? "+
                "group by month";

        connection.query(sql,[year,club,eventType],function (err,result){
        connection.release();   

        if (err) callback(err);
        else callback(null,result);
        });
        
        }
        
    });
};  